<?php

namespace App\Entity;

use App\Repository\ReservationsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReservationsRepository::class)
 */
class Reservations
{


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $tel;

    /**
     * @ORM\Column(type="datetime")
     */
    private $horaireDemande;

    /**
     * @ORM\Column(type="integer")
     */
    private $placesDemandees;

    /**
     * @ORM\Column(name="statut", type="string", length=255)
     */
    private $statut= 'en attente';

    /**
     * @ORM\ManyToOne(targetEntity=Etablissements::class, inversedBy="reservations")
     */
    private $etablissement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getHoraireDemande(): ?\DateTimeInterface
    {
        return $this->horaireDemande;
    }

    public function setHoraireDemande(\DateTimeInterface $horaireDemande): self
    {
        $this->horaireDemande = $horaireDemande;

        return $this;
    }

    public function getPlacesDemandees(): ?int
    {
        return $this->placesDemandees;
    }

    public function setPlacesDemandees(int $placesDemandees): self
    {
        $this->placesDemandees = $placesDemandees;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getEtablissement(): ?Etablissements
    {
        return $this->etablissement;
    }

    public function setEtablissement(?Etablissements $etablissement): self
    {
        $this->etablissement = $etablissement;

        return $this;
    }
}
