<?php

namespace App\Entity;

use App\Repository\EtablissementsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EtablissementsRepository::class)
 */
class Etablissements
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="veuillez saisir un nom d'établisssement")
     */
    private $nom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive(message="veuillez saisir un nombre positif")
     * @Assert\GreaterThanOrEqual(value="5", message="il faut 5 places minimum")
     */
    private $placesTotales;

    /**
     * @ORM\Column(type="boolean")
     */
    private $reservationPossible;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresseLigne1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity=Reservations::class, mappedBy="etablissement")
     */
    private $reservations;

    /**
     * @ORM\ManyToMany(targetEntity=TypesCuisine::class, mappedBy="etablissements")
     */
    private $typesCuisines;

    /**
     * @ORM\OneToMany(targetEntity=Images::class, mappedBy="etablissement", orphanRemoval=true, cascade={"persist"})
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity=Tags::class, mappedBy="etablissement")
     */
    private $tags;



    public function __construct()
    {
        $this->reservations = new ArrayCollection();
        $this->typesCuisines = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPlacesTotales(): ?int
    {
        return $this->placesTotales;
    }

    public function setPlacesTotales(int $placesTotales): self
    {
        $this->placesTotales = $placesTotales;

        return $this;
    }

    public function getReservationPossible(): ?bool
    {
        return $this->reservationPossible;
    }

    public function setReservationPossible(bool $reservationPossible): self
    {
        $this->reservationPossible = $reservationPossible;

        return $this;
    }

    public function getAdresseLigne1(): ?string
    {
        return $this->adresseLigne1;
    }

    public function setAdresseLigne1(string $adresseLigne1): self
    {
        $this->adresseLigne1 = $adresseLigne1;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection|Reservations[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservations $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setEtablissement($this);
        }

        return $this;
    }

    public function removeReservation(Reservations $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getEtablissement() === $this) {
                $reservation->setEtablissement(null);
            }
        }

        return $this;
    }



    public function __toString()
    {
        return $this->getNom().' à '.$this->getVille();
    }

    /**
     * @return Collection|TypesCuisine[]
     */
    public function getTypesCuisines(): Collection
    {
        return $this->typesCuisines;
    }

    public function addTypesCuisine(TypesCuisine $typesCuisine): self
    {
        if (!$this->typesCuisines->contains($typesCuisine)) {
            $this->typesCuisines[] = $typesCuisine;
            $typesCuisine->addEtablissement($this);
        }

        return $this;
    }

    public function removeTypesCuisine(TypesCuisine $typesCuisine): self
    {
        if ($this->typesCuisines->removeElement($typesCuisine)) {
            $typesCuisine->removeEtablissement($this);
        }

        return $this;
    }

    /**
     * @return Collection|Images[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Images $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setEtablissement($this);
        }

        return $this;
    }

    public function removeImage(Images $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getEtablissement() === $this) {
                $image->setEtablissement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tags[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tags $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addEtablissement($this);
        }

        return $this;
    }

    public function removeTag(Tags $tag): self
    {
        if ($this->tags->removeElement($tag)) {
            $tag->removeEtablissement($this);
        }

        return $this;
    }


}
