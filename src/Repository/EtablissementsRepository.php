<?php

namespace App\Repository;

use App\Entity\Etablissements;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Etablissements|null find($id, $lockMode = null, $lockVersion = null)
 * @method Etablissements|null findOneBy(array $criteria, array $orderBy = null)
 * @method Etablissements[]    findAll()
 * @method Etablissements[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EtablissementsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Etablissements::class);
    }


    public function paginate($nbElements, $page){
        return $this->createQueryBuilder('e')
            ->setMaxResults($nbElements)
            ->setFirstResult(($nbElements * $page) - $nbElements)
            ->getQuery()
            ->getResult();
    }

    public function getAllJoined(){
        return $this->createQueryBuilder('e')
            ->leftJoin('e.tags', 't')
            ->leftJoin('e.typesCuisines', 'c')
            ->leftJoin('e.images', 'i')
            ->leftJoin('e.reservations', 'r')
            ->addSelect('t,c,i, SUM(r.placesDemandees) as placesDemandees')
            ->groupBy('e,t,c,i')
            ->orderBy('e.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getFiltered($filtres)
    {

        $query= $this->createQueryBuilder('e')
            ->leftJoin('e.tags', 't')
            ->leftJoin('e.typesCuisines', 'c')
            ->leftJoin('e.images', 'i')
            ->leftJoin('e.reservations', 'r')
            //            ->addSelect('t,c,i');
            ->addSelect('t,c,i, SUM(r.placesDemandees) as placesDemandees')
            ->groupBy('e,t,c,i');


        $typesCuisines= $filtres['types']->toArray();
        $tags= $filtres['tags']->toArray();
//        dump($typesCuisines);
//        dump($tags);
//        die();

        if (count($tags)){

//            $query->andWhere('t IN (:tags)')
            $query->andWhere(':tags MEMBER OF e.tags')  // e.tags représente la relation entre etablissemnts et tags,  l'utilisation de l'alias des lefts join ne fonctionne pas
                ->setParameter('tags', $tags);
        }

        if (count($typesCuisines)){
            $query->andWhere(':types_cuisine MEMBER OF e.typesCuisines')
                ->setParameter('types_cuisine', $typesCuisines);
        }

        $query->andWhere('e.reservationPossible = :reservation_possible')
            ->setParameter('reservation_possible', $filtres["reservation_possible"]);

        $query->orderBy('e.id', 'DESC');


        return $query->getQuery()->getResult();


    }

    // /**
    //  * @return Etablissements[] Returns an array of Etablissements objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Etablissements
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
