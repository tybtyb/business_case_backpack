<?php

namespace App\Repository;

use App\Entity\TypesCuisine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypesCuisine|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypesCuisine|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypesCuisine[]    findAll()
 * @method TypesCuisine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypesCuisineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypesCuisine::class);
    }

    // /**
    //  * @return TypesCuisine[] Returns an array of TypesCuisine objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypesCuisine
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
