<?php

namespace App\Form;

use App\Entity\Reservations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
//        dd($options);

        $builder

            ->add('prenom')
            ->add('nom')
            ->add('tel')
            ->add('horaireDemande', DateTimeType::class, [
                'date_widget'=> 'single_text',
                'time_widget'=> 'single_text',
                'html5' => false,
            ])
            ->add('placesDemandees')
//            ->add('statut') // valeur par défaut dans le mapping de l'entité
//            ->add('etablissement')
            ->add('Valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservations::class,
        ]);
    }
}
