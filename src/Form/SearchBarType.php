<?php

namespace App\Form;

use App\Entity\Etablissements;
use App\Entity\Tags;
use App\Entity\TypesCuisine;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchBarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('types', EntityType::class, [
                'class' => TypesCuisine::class,
                'choice_label' => 'nom',
//                'placeholder' => 'Choisissez un type de cuisine',
                'required' => false,
                // used to render a select box, check boxes or radios
                 'multiple' => true,
                 'expanded' => true,
            ])
            ->add('tags', EntityType::class, [
                'class' => Tags::class,
                'choice_label' => 'nom',

                // used to render a select box, check boxes or radios
                 'multiple' => true,
                 'expanded' => true,
            ])
            ->add('reservation_possible', CheckboxType::class, [
                'label'    => 'Réservation Possible ?',
                'required' => false,
                'value' => true,
                ])
            ->add('Valider', SubmitType::class);

    }


}