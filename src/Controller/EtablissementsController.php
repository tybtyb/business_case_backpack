<?php

namespace App\Controller;

use App\Entity\Etablissements;
use App\Entity\Images;
use App\Entity\Reservations;
use App\Form\EtablissementsType;
use App\Form\ReservationsType;
use App\Form\SearchBarType;
use App\Repository\EtablissementsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/etablissements')]
class EtablissementsController extends AbstractController
{
    #[Route('/paginate/{elements}/{page}', name: 'pagination', methods: ['GET'])]
    public function paginate($elements, $page, EtablissementsRepository $etablissementsRepository)
    {
        $affichage= $etablissementsRepository->paginate($elements, $page);

        return $this->render('etablissements/pagination.html.twig', [
            'nb_elements' => $elements,
            'page' => $page,
            'affichage' => $affichage,
            'total' => $etablissementsRepository->count([]),
        ]);
    }

    #[Route('/', name: 'etablissements_index', methods: ['GET', 'POST'])]
    public function index(Request $request, EtablissementsRepository $etablissementsRepository): Response
    {
        // test dql pour optimiser la requête pour la page de résultats
//        dump($etablissementsRepository->getAllJoined());
//        die();

        $form = $this->createForm(SearchBarType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $filtres= $form->getData();
//            dd($filtres);
            $etablissements= $etablissementsRepository->getFiltered($filtres);
//            dd($etablissements);
        }else {
            $etablissements= $etablissementsRepository->getAllJoined();
        }

//        dd($etablissements);

            return $this->render('etablissements/index.html.twig', [
            'etablissements' => $etablissements,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/new', name: 'etablissements_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, SluggerInterface $slugger): Response
    {
        $etablissement = new Etablissements();
        $form = $this->createForm(EtablissementsType::class, $etablissement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//            dd($form);


            $etablissement= $form->getData();

                $i=0;
                foreach ($form->get('images') as $img){
                    $image= $img->get('imageLink')->getData();
//                    dd($image);
                    $originalFileName= pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                    $safeFileName= $slugger->slug($originalFileName);
                    $newFileName= $safeFileName.'-'.uniqid().'.'.$image->guessExtension();

                    $image->move(
                        $this->getParameter('images_restos'),
                        $newFileName
                    );

                    $etablissement->getImages()[$i]->setuniqId($newFileName);
                    $etablissement->getImages()[$i]->setEtablissement($etablissement);
                    $i+=1;
                }

//                $inBddImages = new Images();
//                $inBddImages->setEtablissement($etablissement);
//                $inBddImages->setUniqId($newFileName);


            $entityManager->persist($etablissement);
            $entityManager->flush();

            return $this->redirectToRoute('etablissements_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('etablissements/new.html.twig', [
            'etablissement' => $etablissement,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'etablissements_show', methods: ['GET', 'POST'])]
    public function show(Etablissements $etablissement, Request $request,EntityManagerInterface $entityManager): Response
    {
        $reservation = new Reservations();
        $form = $this->createForm(ReservationsType::class, $reservation, ['attr' => ['id' =>$etablissement->getId()]]);
//        dd($request);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reservation->setEtablissement($etablissement);
            $entityManager->persist($reservation);
            $entityManager->flush();

            return $this->redirectToRoute('reservations_index', [], Response::HTTP_SEE_OTHER);
        }


        return $this->render('etablissements/show.html.twig', [
            'etablissement' => $etablissement,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}/edit', name: 'etablissements_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Etablissements $etablissement, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(EtablissementsType::class, $etablissement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('etablissements_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('etablissements/edit.html.twig', [
            'etablissement' => $etablissement,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'etablissements_delete', methods: ['POST'])]
    public function delete(Request $request, Etablissements $etablissement, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$etablissement->getId(), $request->request->get('_token'))) {
            $entityManager->remove($etablissement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('etablissements_index', [], Response::HTTP_SEE_OTHER);
    }
}
