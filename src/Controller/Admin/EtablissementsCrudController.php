<?php

namespace App\Controller\Admin;

use App\Entity\Etablissements;
use App\Form\ImageType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;

class EtablissementsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Etablissements::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
//            IdField::new('id'),
            TextField::new('nom'),
            TextEditorField::new('description'),
            IntegerField::new('placesTotales'),
            BooleanField::new('reservationPossible'),
            TextField::new('adresseLigne1'),
            TextField::new('codePostal'),
            TextField::new('ville'),
            CollectionField::new('images')
                ->setEntryType(ImageType::class)
                ->allowAdd()
                ->allowDelete(),
        ];
    }

}
