<?php

namespace App\Controller\Admin;

use App\Entity\Etablissements;
use App\Entity\Images;
use App\Entity\Reservations;
use App\Entity\Tags;
use App\Entity\TypesCuisine;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Business Case Backpack');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Etablissements', 'fas fa-list', Etablissements::class);
        yield MenuItem::linkToCrud('Reservations', 'far fa-calendar-check', Reservations::class);
        yield MenuItem::linkToCrud('Images', 'far fa-calendar-check', Images::class);
        yield MenuItem::linkToCrud('Tags', 'far fa-calendar-check', Tags::class);
        yield MenuItem::linkToCrud('Types de cuisine', 'far fa-calendar-check', TypesCuisine::class);
    }
}
