<?php

namespace App\Controller\Admin;

use App\Entity\TypesCuisine;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TypesCuisineCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TypesCuisine::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
