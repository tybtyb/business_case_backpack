<?php

namespace App\Controller\Admin;

use App\Entity\Etablissements;
use App\Entity\Reservations;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ReservationsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Reservations::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
//            IdField::new('id'),
            'prenom',
            'nom',
            'tel',
            'horaireDemande',
            'placesDemandees',
            TextField::new('statut')
                ->onlyOnDetail()
                ->onlyOnIndex(),

            AssociationField::new('etablissement')
                ->formatValue( function ($value, Reservations $reservations){
                    return $reservations->getEtablissement();
                }),
            AssociationField::new('etablissement', 'etablissement_id')
                ->formatValue( function ($value, Reservations $reservations){
                    return $reservations->getEtablissement()->getId();
                }),


        ];
    }

}
