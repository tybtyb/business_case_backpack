<?php

namespace App\Controller;

use App\Entity\TypesCuisine;
use App\Form\TypesCuisineType;
use App\Repository\TypesCuisineRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/typesCuisine')]
class TypesCuisineController extends AbstractController
{
    #[Route('/', name: 'types_cuisine_index', methods: ['GET'])]
    public function index(TypesCuisineRepository $typesCuisineRepository): Response
    {
        return $this->render('types_cuisine/index.html.twig', [
            'types_cuisines' => $typesCuisineRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'types_cuisine_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $typesCuisine = new TypesCuisine();
        $form = $this->createForm(TypesCuisineType::class, $typesCuisine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($typesCuisine);
            $entityManager->flush();

            return $this->redirectToRoute('types_cuisine_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('types_cuisine/new.html.twig', [
            'types_cuisine' => $typesCuisine,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'types_cuisine_show', methods: ['GET'])]
    public function show(TypesCuisine $typesCuisine): Response
    {
        return $this->render('types_cuisine/show.html.twig', [
            'types_cuisine' => $typesCuisine,
        ]);
    }

    #[Route('/{id}/edit', name: 'types_cuisine_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, TypesCuisine $typesCuisine, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TypesCuisineType::class, $typesCuisine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('types_cuisine_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('types_cuisine/edit.html.twig', [
            'types_cuisine' => $typesCuisine,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'types_cuisine_delete', methods: ['POST'])]
    public function delete(Request $request, TypesCuisine $typesCuisine, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typesCuisine->getId(), $request->request->get('_token'))) {
            $entityManager->remove($typesCuisine);
            $entityManager->flush();
        }

        return $this->redirectToRoute('types_cuisine_index', [], Response::HTTP_SEE_OTHER);
    }
}
