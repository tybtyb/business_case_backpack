<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Annotation\Route;
use Faker;




class TestController extends AbstractController
{
    #[Route('/test', name: 'test')]
    public function index(): Response
    {
        // use the factory to create a Faker\Generator instance
        $faker = Faker\Factory::create();
// generate data by calling methods
        $faker1=$faker->name();
// 'Vince Sporer'
        $faker2=$faker->email();
// 'walter.sophia@hotmail.com'
        $faker3=$faker->text();
// 'Numquam ut mollitia at consequuntur inventore dolorem.'
        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
            'faker1' => $faker1,
            'faker2' => $faker2,
            'faker3' => $faker3,
        ]);
    }

#[Route('/test/scandir', name: 'scandir')]
    public function testScanDir()
    {
        $scan = scanDir('../public/assets/images/bibliotheque_images');
        return $this->render('test/scan.html.twig', [
            'controller_name' => 'TestController',
            'scan' => $scan,
        ]);
    }

}
