<?php

namespace App\DataFixtures;

use App\Entity\Etablissements;
use App\Repository\ImagesRepository;
use App\Repository\TagsRepository;
use App\Repository\TypesCuisineRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class EtablissementsFixtures extends Fixture implements OrderedFixtureInterface
{

   private $tagsRepository;
   private $typesCuisineRepository;
   private $imagesRepository;

    public function __construct(TagsRepository $tagsRepository, TypesCuisineRepository $typesCuisineRepository, ImagesRepository $imagesRepository){

        $this->tagsRepository= $tagsRepository;
        $this->typesCuisineRepository= $typesCuisineRepository;
        $this->imagesRepository= $imagesRepository;

    }



    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        // use the factory to create a Faker\Generator instance
        $faker = Faker\Factory::create('fr_FR');
        // generate data by calling methods
        $faker1=$faker->streetAddress;
        //dump($faker1);

        // recupération de tous les tags et de tous les types de cuisine
        $tags= $this->tagsRepository->findAll();
        $types= $this->typesCuisineRepository->findAll();
        $images= $this->imagesRepository->findAll();

        dump($images);


        for ($i=1; $i<=50; $i++){
            $etablissement= new Etablissements();
            $etablissement->setNom($faker->realText(30, 2));
            $etablissement->setDescription($faker->text(1500));
            $etablissement->setPlacesTotales($faker->numberBetween(15, 110));
            $etablissement->setReservationPossible($faker->boolean);
            $etablissement->setAdresseLigne1($faker->streetAddress);
            $etablissement->setCodePostal($faker->postcode);
            $etablissement->setVille($faker->city);
            for ($j=1; $j<=rand(1,3); $j++ ){
                $etablissement->addTag($tags[rand(0,9)]);
            }
            for ($j=1; $j<=rand(1,2); $j++ ){
                $etablissement->addTypesCuisine($types[rand(0,9)]);
            }
            for ($j=1; $j<=rand(5,15); $j++ ){
                $rand= rand(0, count($images)-1);
                $randomImage=$images[$rand];
                $etablissement->addImage($randomImage);
                array_splice($images, $rand, 1);

            }

            $manager->persist($etablissement);
            $manager->flush();
        }



    }


    public function getOrder()
    {
        return 4;
    }
}
