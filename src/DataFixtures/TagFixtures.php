<?php

namespace App\DataFixtures;

use App\Entity\Tags;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TagFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        for ($i=1; $i<=10; $i++ ){
            $tag= new Tags();
            $tag->setNom('tag '.$i);
            $manager->persist($tag);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
