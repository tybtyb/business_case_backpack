<?php

namespace App\DataFixtures;

use App\Entity\Images;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ImagesFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        // pas de chemin relatif par rapport au dossier fixtures, l'éxécution de la fixture elle même à l'air de s'effectuer à la racine du projet
        $scan = scanDir('public/assets/images/bibliotheque_images');

        for ($i=0; $i<count($scan); $i++){
            $image= new Images();
            //test copie des fichiers utilisées par rand() dans le dossier upload
            $rand= rand(3, count($scan)-1);
            $fileToCopy= $scan[$rand]; // string normalement
            $image->setUniqId($scan[$rand]);

            copy('public/assets/images/bibliotheque_images/'.$fileToCopy.'', 'public/uploads/imagesResto/'.$fileToCopy.''  );
            // voir pour implémenter une vérification de la copy (copy() renvoi true ou false en fonction du succès de la copy)
            $manager->persist($image);

        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
