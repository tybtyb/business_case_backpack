<?php

namespace App\DataFixtures;

use App\Entity\Tags;
use App\Entity\TypesCuisine;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TypesCuisineFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        for ($i=1; $i<=10; $i++ ){
            $typeCuisine= new TypesCuisine();
            $typeCuisine->setNom('typeCuisine '.$i);
            $manager->persist($typeCuisine);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
