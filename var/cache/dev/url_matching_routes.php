<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'admin', '_controller' => 'App\\Controller\\Admin\\DashboardController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null]],
        '/etablissements' => [[['_route' => 'etablissements_index', '_controller' => 'App\\Controller\\EtablissementsController::index'], null, ['GET' => 0, 'POST' => 1], null, true, false, null]],
        '/etablissements/new' => [[['_route' => 'etablissements_new', '_controller' => 'App\\Controller\\EtablissementsController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/reservations' => [[['_route' => 'reservations_index', '_controller' => 'App\\Controller\\ReservationsController::index'], null, ['GET' => 0], null, true, false, null]],
        '/reservations/new' => [[['_route' => 'reservations_new', '_controller' => 'App\\Controller\\ReservationsController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/test' => [[['_route' => 'test', '_controller' => 'App\\Controller\\TestController::index'], null, null, null, false, false, null]],
        '/test/scandir' => [[['_route' => 'scandir', '_controller' => 'App\\Controller\\TestController::testScanDir'], null, null, null, false, false, null]],
        '/typesCuisine' => [[['_route' => 'types_cuisine_index', '_controller' => 'App\\Controller\\TypesCuisineController::index'], null, ['GET' => 0], null, true, false, null]],
        '/typesCuisine/new' => [[['_route' => 'types_cuisine_new', '_controller' => 'App\\Controller\\TypesCuisineController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|wdt/([^/]++)(*:24)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:69)'
                            .'|router(*:82)'
                            .'|exception(?'
                                .'|(*:101)'
                                .'|\\.css(*:114)'
                            .')'
                        .')'
                        .'|(*:124)'
                    .')'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:159)'
                .')'
                .'|/etablissements/(?'
                    .'|paginate/([^/]++)/([^/]++)(*:213)'
                    .'|([^/]++)(?'
                        .'|(*:232)'
                        .'|/edit(*:245)'
                        .'|(*:253)'
                    .')'
                .')'
                .'|/reservations/([^/]++)(?'
                    .'|(*:288)'
                    .'|/edit(*:301)'
                    .'|(*:309)'
                .')'
                .'|/typesCuisine/([^/]++)(?'
                    .'|(*:343)'
                    .'|/edit(*:356)'
                    .'|(*:364)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        24 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        69 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        82 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        101 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        114 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        124 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        159 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        213 => [[['_route' => 'pagination', '_controller' => 'App\\Controller\\EtablissementsController::paginate'], ['elements', 'page'], ['GET' => 0], null, false, true, null]],
        232 => [[['_route' => 'etablissements_show', '_controller' => 'App\\Controller\\EtablissementsController::show'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        245 => [[['_route' => 'etablissements_edit', '_controller' => 'App\\Controller\\EtablissementsController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        253 => [[['_route' => 'etablissements_delete', '_controller' => 'App\\Controller\\EtablissementsController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        288 => [[['_route' => 'reservations_show', '_controller' => 'App\\Controller\\ReservationsController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        301 => [[['_route' => 'reservations_edit', '_controller' => 'App\\Controller\\ReservationsController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        309 => [[['_route' => 'reservations_delete', '_controller' => 'App\\Controller\\ReservationsController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        343 => [[['_route' => 'types_cuisine_show', '_controller' => 'App\\Controller\\TypesCuisineController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        356 => [[['_route' => 'types_cuisine_edit', '_controller' => 'App\\Controller\\TypesCuisineController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        364 => [
            [['_route' => 'types_cuisine_delete', '_controller' => 'App\\Controller\\TypesCuisineController::delete'], ['id'], ['POST' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
