<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211119135923 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE types_cuisine (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE etablissements ADD types_cuisine_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE etablissements ADD CONSTRAINT FK_29F65EB1D8221D84 FOREIGN KEY (types_cuisine_id) REFERENCES types_cuisine (id)');
        $this->addSql('CREATE INDEX IDX_29F65EB1D8221D84 ON etablissements (types_cuisine_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etablissements DROP FOREIGN KEY FK_29F65EB1D8221D84');
        $this->addSql('DROP TABLE types_cuisine');
        $this->addSql('DROP INDEX IDX_29F65EB1D8221D84 ON etablissements');
        $this->addSql('ALTER TABLE etablissements DROP types_cuisine_id');
    }
}
