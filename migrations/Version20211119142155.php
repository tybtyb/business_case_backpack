<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211119142155 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etablissements DROP FOREIGN KEY FK_29F65EB1D8221D84');
        $this->addSql('DROP INDEX IDX_29F65EB1D8221D84 ON etablissements');
        $this->addSql('ALTER TABLE etablissements DROP types_cuisine_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etablissements ADD types_cuisine_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE etablissements ADD CONSTRAINT FK_29F65EB1D8221D84 FOREIGN KEY (types_cuisine_id) REFERENCES types_cuisine (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_29F65EB1D8221D84 ON etablissements (types_cuisine_id)');
    }
}
