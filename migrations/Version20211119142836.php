<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211119142836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE types_cuisine_etablissements (types_cuisine_id INT NOT NULL, etablissements_id INT NOT NULL, INDEX IDX_8D8DCC27D8221D84 (types_cuisine_id), INDEX IDX_8D8DCC27D23B76CD (etablissements_id), PRIMARY KEY(types_cuisine_id, etablissements_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE types_cuisine_etablissements ADD CONSTRAINT FK_8D8DCC27D8221D84 FOREIGN KEY (types_cuisine_id) REFERENCES types_cuisine (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE types_cuisine_etablissements ADD CONSTRAINT FK_8D8DCC27D23B76CD FOREIGN KEY (etablissements_id) REFERENCES etablissements (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE types_cuisine_etablissements');
    }
}
